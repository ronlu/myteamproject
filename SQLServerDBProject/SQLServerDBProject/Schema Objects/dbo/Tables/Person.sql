﻿CREATE TABLE [dbo].[Person]
(
	[Id] INT NOT NULL PRIMARY KEY,
	FirstName varchar(30) Null,
	LastName varchar(30), 
	SSN varchar(9) Masked WITH ( FUNCTION = 'partial(1, "XXXXX", 0)') NOT NULL 
);
