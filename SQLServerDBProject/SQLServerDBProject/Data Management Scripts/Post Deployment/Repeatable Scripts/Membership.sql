﻿Begin Tran
DECLARE @SummaryOfChanges TABLE(Change VARCHAR(20));

Merge Into [dbo].[Membership] with (HOLDLOCK) AS Target
USING  
    ( VALUES 
	    (1, 'Roberto', 'Tamburello', '555.123.4567', 'RTamburello@contoso.com'), 
	    (2, 'Janice', 'Galvin', '555.123.4568', 'JGalvin@contoso.com.co'),  
        (3, 'Zheng', 'Mu', '555.123.4569', 'ZMu@contoso.net')
	)      
    AS Source (nMemberId, nFirstName, nLastName, nPhone, nEmail )
ON Target.MemberID = Source.nMemberID
WHEN Matched Then UPDATE
   set FirstName = Source.nFirstName, 
	   LastName = Source.nLastName, 
	   Phone = Source.nPhone, 
	   Email = Source.nEmail
WHEN Not Matched By Target Then
   insert (MemberId, FirstName, LastName, Phone, Email) 
   Values (nMemberId, nFirstName, nLastName, nPhone, nEmail)
OUTPUT $action INTO @SummaryOfChanges;  

-- Query the results of the table variable.  
SELECT Change, COUNT(*) AS CountPerChange  
FROM @SummaryOfChanges  
GROUP BY Change;  
Commit;