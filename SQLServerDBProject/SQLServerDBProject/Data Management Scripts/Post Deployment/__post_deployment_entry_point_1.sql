﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

GO
 PRINT '**************************************************'
 PRINT '***** running repetable scripts ******************'
 PRINT '**************************************************'

:r _repetable_scripts_loader.sql

GO

 PRINT '**************************************************'
 PRINT '***** sync database sequences to ref table********'
 PRINT '**************************************************'

 -- EXEC dbo.database_RestartSequenceNumbers @debug = 0 
 
 GO